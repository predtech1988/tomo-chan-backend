from django.contrib import admin
from tomo_chan.api.admin import SessionAdmin, TagAdmin
from tomo_chan.api.models import Session, Tag

from .models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ["id", "username", "email", "created_at"]

admin.site.register(User, UserAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Session, SessionAdmin)

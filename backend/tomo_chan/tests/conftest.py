import pytest
from rest_framework.test import APIClient
from tomo_chan.users.models import User


@pytest.fixture
def user(db) -> User:
    user = User.objects.create_user(
        username="SonGoku", email="goku@saiyans.com", password="KaMeHaMeHa"
    )
    user.is_verified = True
    user.is_active = True
    user.save()
    return user


@pytest.fixture
def client() -> APIClient:
    return APIClient()


@pytest.fixture
def authorized_client(client, user, tags) -> APIClient:
    resp = client.post(
        "/user/token", {"email": user.email, "password": "KaMeHaMeHa"}, format="json"
    )
    access_token = resp.json()["access"]
    client.credentials(HTTP_AUTHORIZATION="Bearer " + access_token)
    for tag in tags:
        client.post("/api/v1/tags", tag, format="json")
    return client


@pytest.fixture
def tag(faker):
    tag = {
        "tag_id":faker.pyint(),
        "name": faker.word().title(),
        "score": faker.pyint(),
        "is_synced": faker.pybool(),
    }
    return tag


@pytest.fixture
def tags(faker):
    tags: dict = []
    for _ in range(10):
        tag = {
            "tag_id":_ + 1,
            "name": faker.word().title(),
            "score": faker.pyint(),
            "is_synced": faker.pybool(),
        }
        tags.append(tag)
    return tags


@pytest.fixture
def first_item(authorized_client):
    resp = authorized_client.get("/api/v1/tags", content_type="application/json")
    first_item = resp.json()[0]
    return first_item

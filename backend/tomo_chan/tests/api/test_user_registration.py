import pytest
from rest_framework.test import APIClient
from tomo_chan.users.models import User


@pytest.mark.django_db
def test_user_reg_ok():
    """
    Creating new user with right credentials
    """
    obj = {
        "email": "goku@saiyans.com",
        "username": "SonGoku",
        "password": "KaMeHaMeHa",
    }
    client = APIClient()
    client.post("/user/register", obj)
    user = User.objects.first()
    assert user.username == obj["username"]
    assert user.email == obj["email"]


@pytest.mark.django_db
def test_user_reg_no_email():
    obj = {
        "email": "",
        "username": "SonGoku",
        "password": "KaMeHaMeHa",
    }
    client = APIClient()
    resp = client.post("/user/register", obj, format="json")
    assert resp.json() == {"email": ["This field may not be blank."]}


@pytest.mark.django_db
def test_user_reg_bad_email():
    obj = {
        "email": "goku@saiyans",
        "username": "SonGoku",
        "password": "KaMeHaMeHa",
    }
    client = APIClient()
    resp = client.post("/user/register", obj, format="json")
    assert resp.json() == {"email": ["Enter a valid email address."]}


@pytest.mark.django_db
def test_user_reg_no_username():
    obj = {
        "email": "goku@saiyans.com",
        "username": "",
        "password": "KaMeHaMeHa",
    }
    client = APIClient()
    resp = client.post("/user/register", obj, format="json")
    assert resp.json() == {"username": ["This field may not be blank."]}


@pytest.mark.django_db
def test_user_reg_bad_username():
    """
    Username is cheked for alphanumeric character. A-z 0-9 only!
    """
    obj = {
        "email": "goku@saiyans.com",
        "username": "Son-Goku",
        "password": "KaMeHaMeHa",
    }
    client = APIClient()
    resp = client.post("/user/register", obj, format="json")
    assert resp.json() == {
        "non_field_errors": [
            "The username should only contain alphanumeric " "characters"
        ]
    }


@pytest.mark.django_db
def test_user_reg_no_password():
    obj = {
        "email": "goku@saiyans.com",
        "username": "SonGoku",
        "password": "",
    }
    client = APIClient()
    resp = client.post("/user/register", obj, format="json")
    assert resp.json() == {"password": ["This field may not be blank."]}


@pytest.mark.django_db
def test_user_reg_short_password():
    obj = {
        "email": "goku@saiyans.com",
        "username": "SonGoku",
        "password": "dbz",
    }
    client = APIClient()
    resp = client.post("/user/register", obj, format="json")
    assert resp.json() == {"password": ["Ensure this field has at least 6 characters."]}


@pytest.mark.django_db
def test_user_reg_same_email():
    obj = {
        "email": "goku@saiyans.com",
        "username": "SonGoku",
        "password": "KaMeHaMeHa",
    }
    client = APIClient()
    client.post("/user/register", obj)
    obj["username"] = "Bulma"
    resp = client.post("/user/register", obj)
    assert resp.json() == {"email": ["user with this email already exists."]}


@pytest.mark.django_db
def test_user_reg_same_username():
    obj = {
        "email": "goku@saiyans.com",
        "username": "SonGoku",
        "password": "KaMeHaMeHa",
    }
    client = APIClient()
    client.post("/user/register", obj)
    obj["email"] = "bulma@capsule-corp.com"
    resp = client.post("/user/register", obj)
    assert resp.json() == {"username": ["user with this username already exists."]}


@pytest.mark.django_db
def test_user_reg_invalid_email():
    """
    Email with inavlid character "_"
    """
    obj = {
        "email": "bulma@capsule_corp.com",
        "username": "Bulma",
        "password": "LoveMoney",
    }
    client = APIClient()
    resp = client.post("/user/register", obj)
    assert resp.json() == {"email": ["Enter a valid email address."]}

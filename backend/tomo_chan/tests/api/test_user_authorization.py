import pytest
from rest_framework.test import APIClient


@pytest.mark.django_db
def test_user_authorization_ok(user):
    """
    Receiving "access" and "refresh" token's
    """
    obj = {"email": user.email, "password": "KaMeHaMeHa"}
    client = APIClient()
    resp = client.post("/user/token", obj, format="json")

    assert resp.status_code == 200


@pytest.mark.django_db
def test_user_authorization_notexisting_email(user):
    obj = {"email": "wrong@email.com", "password": user.password}
    client = APIClient()
    resp = client.post("/user/token", obj, format="json")

    assert resp.status_code == 401
    assert resp.json() == {
        "detail": "No active account found with the given credentials"
    }


@pytest.mark.django_db
def test_user_authorization_empty_email():
    obj = {"email": "", "password": "KaMeHaMeHa"}
    client = APIClient()
    resp = client.post("/user/token", obj, format="json")

    assert resp.status_code == 400
    assert resp.json() == {"email": ["This field may not be blank."]}


@pytest.mark.django_db
def test_user_authorization_invalid_email():
    """
    Email with inavlid character "_"
    """
    obj = {"email": "bulma@capsule_corp.com", "password": "KaMeHaMeHa"}
    client = APIClient()
    resp = client.post("/user/token", obj, format="json")

    assert resp.status_code == 401
    assert resp.json() == {
        "detail": "No active account found with the given credentials"
    }


@pytest.mark.django_db
def test_user_authorization_empty_password(user):
    obj = {"email": user.email, "password": ""}
    client = APIClient()
    resp = client.post("/user/token", obj, format="json")

    assert resp.status_code == 400
    assert resp.json() == {"password": ["This field may not be blank."]}


@pytest.mark.django_db
def test_user_authorization_short_password(user):
    obj = {"email": user.email, "password": "123"}
    client = APIClient()
    resp = client.post("/user/token", obj, format="json")

    assert resp.status_code == 401
    assert resp.json() == {
        "detail": "No active account found with the given credentials"
    }


@pytest.mark.django_db
def test_user_token_refresh_ok(user):
    """
    Updating "access" token
    """
    client = APIClient()
    resp = client.post(
        "/user/token", {"email": user.email, "password": "KaMeHaMeHa"}, format="json"
    )
    refresh_token = resp.json()["refresh"]
    resp = client.post("/user/token/refresh", {"refresh": refresh_token}, format="json")
    access_token = resp.json()["access"]
    assert resp.status_code == 200
    assert len(access_token) > 100


@pytest.mark.django_db
def test_user_token_refresh_bad_refresh_token():
    client = APIClient()
    resp = client.post(
        "/user/token/refresh", {"refresh": "some invalid token"}, format="json"
    )
    assert resp.status_code == 401
    assert resp.json() == {
        "detail": "Token is invalid or expired",
        "code": "token_not_valid",
    }


@pytest.mark.django_db
def test_user_token_refresh_empty_refresh_token():
    client = APIClient()
    resp = client.post("/user/token/refresh", {"refresh": ""}, format="json")
    assert resp.status_code == 400
    assert resp.json() == {"refresh": ["This field may not be blank."]}


@pytest.mark.django_db
def test_user_token_refresh_no_refresh_token():
    client = APIClient()
    resp = client.post("/user/token/refresh", {"payload": "some data"}, format="json")
    assert resp.status_code == 400
    assert resp.json() == {"refresh": ["This field is required."]}


@pytest.mark.django_db
def test_token_blacklist(user):
    obj = {"email": user.email, "password": "KaMeHaMeHa"}
    client = APIClient()
    resp = client.post("/user/token", obj, format="json")
    refresh_token = resp.json()["refresh"]
    resp = client.post("/user/logout", {"refresh": refresh_token}, format="json")
    assert resp.status_code == 204


@pytest.mark.django_db
def test_using_blacklisted_token(user):
    obj = {"email": user.email, "password": "KaMeHaMeHa"}
    client = APIClient()
    resp = client.post("/user/token", obj, format="json")
    refresh_token = resp.json()["refresh"]
    resp = client.post("/user/logout", {"refresh": refresh_token}, format="json")
    resp = client.post("/user/token/refresh", {"refresh": refresh_token}, format="json")
    assert resp.json() == {"detail": "Token is blacklisted", "code": "token_not_valid"}

import pytest
from rest_framework import status


def test_faker(faker):
    assert isinstance(faker.name(), str)


@pytest.mark.django_db
def test_create_new_tag_no_name_field(authorized_client, tag):
    tag["name"] = None
    resp = authorized_client.post("/api/v1/tags", tag, format="json")
    assert resp.status_code == status.HTTP_400_BAD_REQUEST
    assert resp.json() == {"name": ["This field may not be null."]}

    tag.pop("name")
    resp = authorized_client.post("/api/v1/tags", tag, format="json")
    assert resp.json() == {"name": ["This field is required."]}


@pytest.mark.django_db
def test_create_retrive(authorized_client, tag):
    resp = authorized_client.post("/api/v1/tags", tag, format="json")
    assert resp.status_code == status.HTTP_201_CREATED

    resp = authorized_client.get("/api/v1/tags", content_type="application/json")
    assert resp.status_code == status.HTTP_200_OK
    assert resp.json()[-1]["alias"] == tag["name"]


@pytest.mark.django_db
def test_get_all_tags(authorized_client):
    resp = authorized_client.get("/api/v1/tags", content_type="application/json")
    tag_id = resp.json()[0]["id"]
    assert resp.status_code == status.HTTP_200_OK

    resp = authorized_client.get(
        f"/api/v1/tags/{tag_id}", content_type="application/json"
    )
    assert resp.status_code == status.HTTP_200_OK
    assert resp.json()["owner"]["username"] == "SonGoku"


@pytest.mark.django_db
def test_patch(authorized_client, first_item):
    patched_item = first_item
    patched_item["name"] = "Booba"

    resp = authorized_client.patch(
        f"/api/v1/tags/{first_item['id']}", patched_item, format="json"
    )
    assert resp.status_code == status.HTTP_200_OK
    assert resp.json()["name"] != first_item["name"]


@pytest.mark.django_db
def test_update(authorized_client, first_item):
    patched_item = first_item
    patched_item["name"] = "Booba"
    patched_item["alias"] = "Bimbo"

    resp = authorized_client.put(
        f"/api/v1/tags/{first_item['id']}", patched_item, format="json"
    )
    resp = authorized_client.get(
        f"/api/v1/tags/{first_item['id']}", content_type="application/json"
    )
    assert resp.status_code == status.HTTP_200_OK
    assert resp.json()["name"] == "booba"
    assert resp.json()["alias"] == "Booba"
    assert resp.json()["alias"] != "Bimbo"


@pytest.mark.django_db
def test_delete(authorized_client):
    resp = authorized_client.get("/api/v1/tags", content_type="application/json")
    items_count = len(resp.json())
    resp = authorized_client.delete(
        f"/api/v1/tags/{resp.json()[0]['id']}", format="json"
    )
    assert resp.status_code == status.HTTP_204_NO_CONTENT

    resp = authorized_client.get("/api/v1/tags", content_type="application/json")
    assert len(resp.json()) == items_count - 1

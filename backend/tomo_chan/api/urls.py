from django.urls import path

from .views import TagDetailAPIView, TagsListAPIView

urlpatterns = [
    path("tags", TagsListAPIView.as_view(), name="tags"),
    path("tags/<int:id>", TagDetailAPIView.as_view(), name="tags"),
]

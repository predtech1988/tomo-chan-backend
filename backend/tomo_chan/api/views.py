from rest_framework import generics, permissions, status
from rest_framework.response import Response
from tomo_chan.users.permissions import IsOwner

from .models import Tag
from .serializers import TagSerializer
from .utils import format_tag_name_alias


class TagsListAPIView(generics.ListCreateAPIView):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()
    permission_classes = (permissions.IsAuthenticated, IsOwner)

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user.id)


class TagDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TagSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner)
    queryset = Tag.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)

    def patch(self, request, *args, **kwargs):
        serializer = TagSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        if "name" in request.data:
            request = format_tag_name_alias(request)
            return super().patch(request, *args, **kwargs)
        return Response(
            {"error": "You can update only name field"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    def update(self, request, *args, **kwargs):
        request = format_tag_name_alias(request)
        return super().update(request, *args, **kwargs)

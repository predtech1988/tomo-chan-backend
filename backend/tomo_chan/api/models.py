from django.db import models
from tomo_chan.users.models import User


class Tag(models.Model):
    tag_id = models.IntegerField(db_index=True)
    name = models.CharField(max_length=64)
    owner = models.ForeignKey(to=User, on_delete=models.CASCADE)
    alias = models.CharField(max_length=64, blank=True)
    score = models.IntegerField(default=0)
    is_synced = models.BooleanField(default=False)

    class Meta:
        ordering = ("-score", )


    def __str__(self):
        return self.alias

class Session(models.Model):
    session_date = models.DateTimeField()
    owner = models.ForeignKey(to=User, on_delete=models.CASCADE)
    time_took = models.IntegerField(default=0)
    is_synced = models.BooleanField(default=False)
    tag_1 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    tag_2 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    tag_3 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    tag_4 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    tag_5 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    tag_6 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    tag_7 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    tag_8 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    tag_9 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    tag_10 = models.ForeignKey(to=Tag, on_delete=models.SET_NULL ,null=True, blank=True, related_name='+')
    comment = models.CharField(max_length=512, null=True, blank=True)
    
    class Meta:
        pass
    
    
    def __str__(self):
        rep = f"{self.session_date}, {str(self.owner)}, {self.comment}"
        return rep

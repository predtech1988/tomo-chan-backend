def format_tag_name_alias(request):
    request.data["alias"] = request.data["name"]
    request.data["name"] = request.data["name"].lower()
    return request

from rest_framework import serializers, status
from tomo_chan.users.serializers import UserSerializer

from .models import Tag


class TagSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Tag
        fields = ["id", "tag_id", "name", "alias", "owner"]
        read_only_fields = ["id"]

    def create(self, instance):
        instance["alias"] = instance["name"]
        instance["name"] = instance["name"].lower()
        return super().create(instance)

    # def update(self, instance, validated_data):
    #     return super().update(instance, validated_data)

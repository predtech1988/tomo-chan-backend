from django.contrib import admin


class TagAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "owner", "score"]


class SessionAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "session_date",
        "owner",
        "time_took",
        "is_synced",
        "tag_1",
        "tag_2",
        "tag_3",
        "tag_4",
        "tag_5",
        "tag_6",
        "tag_7",
        "tag_8",
        "tag_9",
        "tag_10",
        "comment",
    ]
